angular.module('lotto')

    .directive('lotto', function () {
        return {
            restrict: 'E',
            scope: {
                title: '@',
                rollN: '@',
                rollFrom: '@',
                extraN: '@',
                extraFrom: '@'
            },
            templateUrl: 'template/lotto-game.html',
            link: function(scope, element, attrs){
                scope.roll = function (amount, outOf) {
                    console.log('rolling '+amount+' out of '+outOf);
                    var n = [], i = 0;

                    for (; ++i <= outOf;) {
                        n.push(i);
                    }

                    for (; --i > amount;) {
                        n.splice(i * Math.random() | 0, 1);
                    }

                    return n;
                };
                scope.play = function(){
                    scope.numbers = scope.roll(scope.rollN, scope.rollFrom);
                    scope.extraNumbers = scope.roll(scope.extraN, scope.extraFrom);
                    console.log('numbers rolled are: '+scope.numbers+', extra: '+scope.extraNumbers);
                };
            }
        };
    });